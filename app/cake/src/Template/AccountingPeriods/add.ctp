<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccountingPeriod $accountingPeriod
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Accounting Periods'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Rulers'), ['controller' => 'Rulers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ruler'), ['controller' => 'Rulers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Months'), ['controller' => 'Months', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Month'), ['controller' => 'Months', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="accountingPeriods form large-9 medium-8 columns content">
    <?= $this->Form->create($accountingPeriod) ?>
    <fieldset>
        <legend><?= __('Add Accounting Period') ?></legend>
        <?php
            echo $this->Form->control('start_ruler_id');
            echo $this->Form->control('start_month_id');
            echo $this->Form->control('start_year_id');
            echo $this->Form->control('start_date');
            echo $this->Form->control('end_ruler_id', ['options' => $rulers, 'empty' => true]);
            echo $this->Form->control('end_month_id', ['options' => $months, 'empty' => true]);
            echo $this->Form->control('end_year_id');
            echo $this->Form->control('end_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
