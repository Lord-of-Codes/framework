<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('User Profile') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Username') ?></th>
                    <td><?= h($user->username) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Email') ?></th>
                    <td><?= h($user->email) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($user->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Admin') ?></th>
                    <td><?= $user->admin ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can Download Hd Images') ?></th>
                    <td><?= $user->can_download_hd_images ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can View Private Catalogues') ?></th>
                    <td><?= $user->can_view_private_catalogues ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can View Private Transliterations') ?></th>
                    <td><?= $user->can_view_private_transliterations ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can Edit Transliterations') ?></th>
                    <td><?= $user->can_edit_transliterations ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can View Private Images') ?></th>
                    <td><?= $user->can_view_private_images ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can View IPadWeb') ?></th>
                    <td><?= $user->can_view_iPadWeb ? __('Yes') : __('No'); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Flash->render() ?>
        <?= $this->Html->link(__('Edit your profile'), ['control' => 'Users', 'action' => 'edit'], ['class' => 'btn-action']) ?>
    </div>

</div>
