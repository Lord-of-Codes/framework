<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Provenience Entity
 *
 * @property int $id
 * @property string|null $provenience
 * @property int|null $region_id
 * @property string|null $geo_coordinates
 *
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Archive[] $archives
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Dynasty[] $dynasties
 */
class Provenience extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'provenience' => true,
        'region_id' => true,
        'geo_coordinates' => true,
        'region' => true,
        'archives' => true,
        'artifacts' => true,
        'dynasties' => true
    ];
}
