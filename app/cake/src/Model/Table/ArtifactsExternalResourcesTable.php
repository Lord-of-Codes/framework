<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsExternalResources Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\ExternalResourcesTable|\Cake\ORM\Association\BelongsTo $ExternalResources
 *
 * @method \App\Model\Entity\ArtifactsExternalResource get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsExternalResource newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArtifactsExternalResource[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsExternalResource|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsExternalResource|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsExternalResource patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsExternalResource[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsExternalResource findOrCreate($search, callable $callback = null, $options = [])
 */
class ArtifactsExternalResourcesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('artifacts_external_resources');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('ExternalResources', [
            'foreignKey' => 'external_resource_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('external_resource_key')
            ->maxLength('external_resource_key', 45)
            ->allowEmpty('external_resource_key');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));
        $rules->add($rules->existsIn(['external_resource_id'], 'ExternalResources'));

        return $rules;
    }
}
