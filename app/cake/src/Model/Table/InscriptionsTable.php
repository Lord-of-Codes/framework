<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Inscriptions Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 *
 * @method \App\Model\Entity\Inscription get($primaryKey, $options = [])
 * @method \App\Model\Entity\Inscription newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Inscription[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Inscription|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Inscription|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Inscription patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Inscription[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Inscription findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InscriptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('inscriptions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('transliteration')
            ->maxLength('transliteration', 4294967295)
            ->allowEmpty('transliteration');

        $validator
            ->scalar('transliteration_clean')
            ->maxLength('transliteration_clean', 4294967295)
            ->allowEmpty('transliteration_clean');

        $validator
            ->scalar('tranliteration_sign_names')
            ->maxLength('tranliteration_sign_names', 4294967295)
            ->allowEmpty('tranliteration_sign_names');

        $validator
            ->nonNegativeInteger('created_by')
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->nonNegativeInteger('credit_to')
            ->allowEmpty('credit_to');

        $validator
            ->boolean('is_latest')
            ->allowEmpty('is_latest');

        $validator
            ->scalar('annotation')
            ->maxLength('annotation', 4294967295)
            ->allowEmpty('annotation');

        $validator
            ->boolean('atf2conll_diff_resolved')
            ->allowEmpty('atf2conll_diff_resolved');

        $validator
            ->boolean('atf2conll_diff_unresolved')
            ->allowEmpty('atf2conll_diff_unresolved');

        $validator
            ->scalar('comments')
            ->allowEmpty('comments');

        $validator
            ->scalar('structure')
            ->allowEmpty('structure');

        $validator
            ->scalar('translations')
            ->allowEmpty('translations');

        $validator
            ->scalar('transcriptions')
            ->allowEmpty('transcriptions');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));

        return $rules;
    }
}
